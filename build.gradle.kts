import name.remal.appendElement
import name.remal.appendTextNode
import name.remal.buildSet
import name.remal.concurrentMapOf
import name.remal.createParentDirectories
import name.remal.default
import name.remal.encodeURIComponent
import name.remal.escapeRegex
import name.remal.findOrAppendElement
import name.remal.forEach
import name.remal.forceDeleteRecursively
import name.remal.gradle_plugins.dsl.extensions.compileOnly
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.convention
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isSnapshotVersion
import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import name.remal.gradle_plugins.integrations.gradle_org.GradleOrgClient
import name.remal.gradle_plugins.plugins.publish.bintray.RepositoryHandlerBintrayExtension
import name.remal.gradle_plugins.plugins.publish.nexus_staging.ReleaseNexusStagingRepository
import name.remal.gradle_plugins.plugins.publish.ossrh.RepositoryHandlerOssrhExtension
import name.remal.isRegularFile
import name.remal.load
import name.remal.loadProperties
import name.remal.nullIfEmpty
import name.remal.remove
import name.remal.retry
import name.remal.store
import name.remal.uncheckedCast
import name.remal.use
import name.remal.walk
import name.remal.writeln
import org.gradle.api.file.DuplicatesStrategy.INCLUDE
import org.gradle.api.publish.plugins.PublishingPlugin.PUBLISH_TASK_GROUP
import org.gradle.internal.os.OperatingSystem
import org.gradle.kotlin.dsl.apply
import org.gradle.util.GradleVersion
import org.gradle.util.GradleVersion.version
import org.gradle.wrapper.GradleUserHomeLookup.DEFAULT_GRADLE_USER_HOME
import java.io.IOException
import java.lang.ProcessBuilder.Redirect.PIPE
import java.lang.System.currentTimeMillis
import java.lang.System.getenv
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Path
import java.time.Duration
import java.util.Properties
import java.util.concurrent.ForkJoinPool
import java.util.concurrent.TimeUnit.MINUTES
import java.util.stream.Stream
import kotlin.collections.set
import kotlin.text.Regex
import kotlin.text.isNotEmpty
import kotlin.text.trim

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath(gradleApi())
        classpath("name.remal", "gradle-plugins", "1.0.211")
    }
}

plugins {
    `java`
    `maven-publish`
}

apply(plugin = "name.remal.default-plugins")
apply(plugin = "name.remal.gitlab-ci")

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

dependencies {
    compileOnly(gradleApi())
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

val versionSuffix = "-SNAPSHOT"
project.group = "name.remal.gradle-api"
project.version = "0" + versionSuffix

configurations["archives"].artifacts.clear()
publishing.publications.clear()

publishing.publications.withType<MavenPublication> {
    groupId = project.group.toString()

    pom {
        url.set("https://gitlab.com/remal/name.remal.gradle-api-artifacts")
        scm {
            url.set("https://gitlab.com/remal/name.remal.gradle-api-artifacts.git")
        }
        developers {
            developer {
                name.set("Semyon Levin")
                email.set("levin.semen@gmail.com")
                id.set(email.get())
            }
        }
        licenses {
            license {
                name.set("Unlicense")
                url.set("https://unlicense.org/UNLICENSE")
            }
        }
    }
}

val BINTRAY_ENABLED = false
val BINTRAY_USER = "remal"
val BINTRAY_PACKAGE = "name.remal"
if (BINTRAY_ENABLED) {
    publishing.repositories.convention[RepositoryHandlerBintrayExtension::class.java].bintray {
        owner = BINTRAY_USER
        repositoryName = "name.remal"
        packageName = BINTRAY_PACKAGE
    }
}

val OSSRH_ENABLED = true
if (OSSRH_ENABLED) {
    publishing.repositories.convention[RepositoryHandlerOssrhExtension::class.java].ossrh()
}

if (!OSSRH_ENABLED || project.isSnapshotVersion) {
    tasks.withType(ReleaseNexusStagingRepository::class.java).configureEach { disableTask() }
}


tasks.forEach {
    if (it is AbstractPublishToMaven || it is GenerateMavenPom || it is GenerateModuleMetadata || it is Sign) {
        it.disableTask()
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

arrayOf("assemble", "check", "build").forEach { taskName ->
    tasks.findByName(taskName)?.disableTask()
}

tasks.withType<AbstractCompile> {
    enabled = false
    setDependsOn(emptyList<Any>())
}

tasks.withType<Jar> {
    destinationDirectory.value(tasks[Jar::class.java, "jar"].destinationDirectory.get())
    duplicatesStrategy = INCLUDE
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

open class ArtifactInfo(
    val name: String,
    val dependenciesMethodName: String? = null,
    val includeLibFilesGlob: String? = null,
    val excludeLibFilesGlob: String? = null,
    val deps: Set<ArtifactInfo> = emptySet(),
    val minGradleVersion: GradleVersion = version("0.0")
) {

    init {
        if (dependenciesMethodName != null && (includeLibFilesGlob != null || excludeLibFilesGlob != null)) {
            throw IllegalArgumentException("${ArtifactInfo::dependenciesMethodName.name} != null and ${ArtifactInfo::includeLibFilesGlob.name}/${ArtifactInfo::excludeLibFilesGlob.name} != null")
        }
    }

    val escapedName = name.replace(Regex("\\W"), "-")

    val includeLibFilesPattern = includeLibFilesGlob
        ?.splitToSequence("**")?.joinToString(".*")
        ?.splitToSequence("*")?.map(::escapeRegex)?.joinToString("[^/]*")
        ?.let(::Regex)

    val excludeLibFilesPattern = excludeLibFilesGlob
        ?.splitToSequence("**")?.joinToString(".*")
        ?.splitToSequence("*")?.map(::escapeRegex)?.joinToString("[^/]*")
        ?.let(::Regex)

    val depNames: Set<String> = deps.mapTo(mutableSetOf(), ArtifactInfo::name)

    val allDeps: Set<ArtifactInfo> = buildSet {
        deps.forEach { dep ->
            if (add(dep)) {
                addAll(dep.allDeps)
            }
        }
    }

    val allDepNames: Set<String> = allDeps.mapTo(mutableSetOf(), ArtifactInfo::name)


    final override fun equals(other: Any?) = this === other || other is ArtifactInfo && name == other.name
    final override fun hashCode() = name.hashCode()
    final override fun toString() = name

}

val allArtifactInfos = mutableListOf<ArtifactInfo>()

allArtifactInfos.add(
    ArtifactInfo(
        name = "local-groovy",
        dependenciesMethodName = "localGroovy"
    )
)

allArtifactInfos.add(
    ArtifactInfo(
        name = "gradle-api",
        dependenciesMethodName = "gradleApi",
        deps = setOf(allArtifactInfos.first { it.name == "local-groovy" })
    )
)

allArtifactInfos.add(
    ArtifactInfo(
        name = "gradle-test-kit",
        dependenciesMethodName = "gradleTestKit",
        deps = setOf(allArtifactInfos.first { it.name == "gradle-api" }),
        minGradleVersion = version("2.6")
    )
)

val EMBEDDED_KOTLIN_ARTIFACT_NAME = "embedded-kotlin"
allArtifactInfos.add(
    ArtifactInfo(
        name = EMBEDDED_KOTLIN_ARTIFACT_NAME,
        includeLibFilesGlob = "kotlin-*.jar",
        excludeLibFilesGlob = "*compiler*",
        deps = setOf(allArtifactInfos.first { it.name == "gradle-api" }),
        minGradleVersion = version("3.5")
    )
)

val CORRESPONDING_KOTLIN_PLUGIN_ARTIFACT_NAME = "corresponding-kotlin-plugin"
allArtifactInfos.add(
    ArtifactInfo(
        name = CORRESPONDING_KOTLIN_PLUGIN_ARTIFACT_NAME,
        includeLibFilesGlob = "kotlin-stdlib-*.jar",
        minGradleVersion = version("3.5")
    )
)

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

val CACHE_EXPIRE_MILLIS = Duration.ofDays(7).toMillis()

val getResponseCodePropertiesFile = buildDir.resolve("response-codes.properties").createParentDirectories()
    .apply { if (exists() && lastModified() < currentTimeMillis() - CACHE_EXPIRE_MILLIS) delete() }
    .apply { createNewFile() }

val getResponseCodeProperties = loadProperties(getResponseCodePropertiesFile)
tasks.withType<AbstractPublishToMaven> {
    onlyIf {
        getResponseCodeProperties.clear()
        synchronized(getResponseCodeProperties) { getResponseCodeProperties.store(getResponseCodePropertiesFile) }
        return@onlyIf true
    }
}

fun getResponseCode(urlString: String): Int {
    getResponseCodeProperties[urlString]?.toString()?.toIntOrNull()?.let { return it }

    val responseCode = retry(10, IOException::class.java, 2_500) {
        val url = URL(urlString)
        val connection = url.openConnection()
        if (connection !is HttpURLConnection) throw IllegalArgumentException("'$urlString' is not a HTTP URL")
        connection.connectTimeout = 2_500
        connection.readTimeout = 5_000
        connection.useCaches = false
        connection.instanceFollowRedirects = true
        connection.requestMethod = "HEAD"
        connection.connect()
        try {
            return@retry connection.responseCode
        } finally {
            connection.disconnect()
        }
    }
    getResponseCodeProperties[urlString] = responseCode.toString()
    synchronized(getResponseCodeProperties) { getResponseCodeProperties.store(getResponseCodePropertiesFile) }
    return responseCode
}

data class IsPublishedCacheKey(val groupId: String, val artifactId: String, val version: String)

val isPublishedToJCenterCache = concurrentMapOf<IsPublishedCacheKey, Boolean>()
fun isPublishedToJCenter(groupId: String = project.group.toString(), artifactId: String, version: String): Boolean {
    val cacheKey = IsPublishedCacheKey(groupId, artifactId, version)
    return isPublishedToJCenterCache.computeIfAbsent(cacheKey) { _ ->
        val pomFileRepositoryPath = arrayOf(
            groupId.replace(".", "/"),
            artifactId,
            version,
            artifactId + "-" + version + ".pom"
        ).joinToString("/")
        val urlString = "https://jcenter.bintray.com/$pomFileRepositoryPath"
        return@computeIfAbsent getResponseCode(urlString) == 200
    }
}

val isPublishedToBintrayCache = concurrentMapOf<IsPublishedCacheKey, Boolean>()
fun isPublishedToBintray(groupId: String = project.group.toString(), artifactId: String, version: String): Boolean {
    val cacheKey = IsPublishedCacheKey(groupId, artifactId, version)
    return isPublishedToBintrayCache.computeIfAbsent(cacheKey) { _ ->
        if (isPublishedToJCenter(groupId, artifactId, version)) {
            return@computeIfAbsent true
        }
        val pomFileRepositoryPath = arrayOf(
            groupId.replace(".", "/"),
            artifactId,
            version,
            artifactId + "-" + version + ".pom"
        ).joinToString("/")
        val urlString = "https://bintray.com/${encodeURIComponent(BINTRAY_USER)}/${encodeURIComponent(BINTRAY_PACKAGE)}/download_file?file_path=${
            encodeURIComponent(
                pomFileRepositoryPath
            )
        }"
        return@computeIfAbsent getResponseCode(urlString) == 200
    }
}

val isPublishedToMavenCentralCache = concurrentMapOf<IsPublishedCacheKey, Boolean>()
fun isPublishedToMavenCentral(
    groupId: String = project.group.toString(),
    artifactId: String,
    version: String
): Boolean {
    val cacheKey = IsPublishedCacheKey(groupId, artifactId, version)
    return isPublishedToMavenCentralCache.computeIfAbsent(cacheKey) { _ ->
        val pomFileRepositoryPath = arrayOf(
            groupId.replace(".", "/"),
            artifactId,
            version
        ).joinToString("/")
        val urlStrings = arrayOf(
            "https://oss.sonatype.org/content/repositories/snapshots/$pomFileRepositoryPath",
            "https://oss.sonatype.org/content/repositories/releases/$pomFileRepositoryPath",
            "https://repo1.maven.org/maven2/$pomFileRepositoryPath"
        )
        return@computeIfAbsent urlStrings.any { getResponseCode(it) == 200 }
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

val minGradleVersionToBuild = maxOf(
    version(
        getenv("GRADLE_MIN_VERSION") ?: findProperty("gradle.min-version")?.toString() ?: "0.0"
    ), version("2.0")
)
val maxGradleVersionToBuild = version(
    getenv("GRADLE_MAX_VERSION") ?: findProperty("gradle.max-version")?.toString() ?: "999999999.0"
)
val REBUILD = getenv("REBUILD")?.toBoolean() == true

val gradleVersions: List<GradleVersion> = run {
    val cacheFile = buildDir.resolve("available-versions.txt")
        .apply { if (exists() && lastModified() < currentTimeMillis() - CACHE_EXPIRE_MILLIS) delete() }

    if (cacheFile.isFile) {
        return@run cacheFile.readText().splitToSequence("\n")
            .map(String::trim)
            .filter(String::isNotEmpty)
            .map(GradleVersion::version)
            .toList()
    } else {
        return@run GradleOrgClient.getAllVersions().asSequence()
            .filter { it.isRelease }
            .map { version(it.version).baseVersion }
            .filter { it in minGradleVersionToBuild..maxGradleVersionToBuild }
            .distinct()
            .sorted()
            .toList()
            .also { gradleVersions ->
                cacheFile.forceDeleteRecursively().createParentDirectories().writeText(
                    gradleVersions.joinToString("\n", transform = GradleVersion::getVersion)
                )
            }
    }
}

ForkJoinPool(20).also { executer ->
    fun createIsPublishedCacheKeysStream(): Stream<IsPublishedCacheKey> = gradleVersions.parallelStream()
        .flatMap { gradleVersion ->
            allArtifactInfos.parallelStream()
                .filter { it.minGradleVersion <= gradleVersion }
                .map { IsPublishedCacheKey(project.group.toString(), it.name, gradleVersion.version + versionSuffix) }
        }

    val tasks = mutableListOf<Callable<Unit>>()
    if (BINTRAY_ENABLED) {
        tasks.add(Callable {
            createIsPublishedCacheKeysStream().forEach {
                isPublishedToBintray(it.groupId, it.artifactId, it.version)
            }
            Unit
        })
    }
    if (OSSRH_ENABLED) {
        tasks.add(Callable {
            createIsPublishedCacheKeysStream().forEach {
                isPublishedToMavenCentral(it.groupId, it.artifactId, it.version)
            }
            Unit
        })
    }
    executer.invokeAll(tasks).forEach { it.get() }

    executer.shutdownNow()
}

gradleVersions.forEach forEachVersion@{ gradleVersion ->
    val artifactInfos = allArtifactInfos.filter { gradleVersion >= it.minGradleVersion }
    if (artifactInfos.isEmpty()) return@forEachVersion

    val version = gradleVersion.version
    val versionDir = buildDir.resolve("versions/$version")

    val escapedVersion = escapeVersion(version)
    val extractedPaths: ExtractedPaths by lazy { extractPaths(artifactInfos, gradleVersion) }

    val sourceJar = tasks.create<Jar>("sourcesJar-$escapedVersion") {
        destinationDirectory.set(versionDir)
        archiveBaseName.set("sources")
        this.archiveVersion.set(version)

        doSetup {
            val srcDir = extractedPaths.distributionPath.resolve("src")
            srcDir.listFiles().default(emptyArray()).asSequence()
                .filter(File::isDirectory)
                .forEach { dir ->
                    this@create.from(dir) {
                        exclude("META-INF/*.MF")
                        exclude("META-INF/*.SF")
                        exclude("META-INF/*.DSA")
                        exclude("META-INF/*.RSA")
                    }
                }
        }
        Unit
    }

    artifactInfos.forEach forEachArtifactInfo@{ artifactInfo ->
        publishing.publications.create<MavenPublication>("$escapedVersion-${artifactInfo.escapedName}") publication@{
            groupId = project.group.toString()
            artifactId = artifactInfo.name
            this.version = version + versionSuffix

            pom {
                name.set("Gradle API artifacts: $version: ${artifactInfo.name}")
                description.set("Gradle API artifacts: $version: ${artifactInfo.name}")

                withXml {
                    val root = asElement()
                    root.getElementsByTagName("dependencyManagement").forEach { it.remove() }
                    root.getElementsByTagName("dependencies").forEach { it.remove() }
                    root.appendElement("dependencies").apply {
                        artifactInfo.deps.forEach { dep ->
                            appendElement("dependency").apply {
                                appendElement("groupId").appendTextNode(this@publication.groupId)
                                appendElement("artifactId").appendTextNode(dep.name)
                                appendElement("version").appendTextNode(this@publication.version)
                            }
                        }
                    }
                }
            }

            if (artifactInfo.name == EMBEDDED_KOTLIN_ARTIFACT_NAME) {
                pom {
                    packaging = "pom"
                    withXml {
                        asElement().findOrAppendElement("dependencies").apply {
                            val paths = extractedPaths.paths[artifactInfo] ?: return@apply
                            val fileNameRegex = Regex("(.+?)-(\\d.*)\\.jar")
                            paths.asSequence()
                                .map(::File)
                                .map(File::getName)
                                .map(fileNameRegex::matchEntire)
                                .filterNotNull()
                                .forEach { matchResult ->
                                    val groupId = "org.jetbrains.kotlin"
                                    val artifactId = matchResult.groupValues[1].default()
                                    val ver = matchResult.groupValues[2].default()
                                    if (BINTRAY_ENABLED && !isPublishedToJCenter(
                                            groupId,
                                            artifactId,
                                            ver
                                        )) throw IllegalStateException("$groupId:$artifactId:$ver is not published to JCenter")
                                    if (OSSRH_ENABLED && !isPublishedToMavenCentral(
                                            groupId,
                                            artifactId,
                                            ver
                                        )) throw IllegalStateException("$groupId:$artifactId:$ver is not published to Maven Central")
                                    appendElement("dependency").apply {
                                        appendElement("groupId").appendTextNode(groupId)
                                        appendElement("artifactId").appendTextNode(artifactId)
                                        appendElement("version").appendTextNode(ver)
                                    }
                                }
                        }
                    }
                }

            } else if (artifactInfo.name == CORRESPONDING_KOTLIN_PLUGIN_ARTIFACT_NAME) {
                pom {
                    packaging = "pom"
                    withXml {
                        asElement().findOrAppendElement("dependencies").apply {
                            val paths = extractedPaths.paths[artifactInfo] ?: return@apply
                            val fileNameRegex = Regex("(.+?)-(\\d.*)\\.jar")
                            val ver = paths.asSequence()
                                .map(::File)
                                .map(File::getName)
                                .map(fileNameRegex::matchEntire)
                                .filterNotNull()
                                .mapNotNull { it.groupValues[2] }
                                .firstOrNull()
                                ?: return@apply

                            arrayOf(
                                "kotlin-gradle-plugin",
                                "kotlin-allopen",
                                "kotlin-noarg",
                                "kotlin-sam-with-receiver"
                            ).forEach { artifactId ->
                                val groupId = "org.jetbrains.kotlin"
                                if (BINTRAY_ENABLED && !isPublishedToJCenter(groupId, artifactId, ver)) {
                                    if (artifactId == "kotlin-gradle-plugin") {
                                        throw IllegalStateException("$groupId:$artifactId:$ver is not published to JCenter")
                                    } else {
                                        return@forEach
                                    }
                                }
                                if (OSSRH_ENABLED && !isPublishedToMavenCentral(groupId, artifactId, ver)) {
                                    if (artifactId == "kotlin-gradle-plugin") {
                                        throw IllegalStateException("$groupId:$artifactId:$ver is not published to Maven Central")
                                    } else {
                                        return@forEach
                                    }
                                }
                                appendElement("dependency").apply {
                                    appendElement("groupId").appendTextNode(groupId)
                                    appendElement("artifactId").appendTextNode(artifactId)
                                    appendElement("version").appendTextNode(ver)
                                }
                            }
                        }
                    }
                }

            } else {
                artifact(tasks.create<Jar>("jar-$escapedVersion-${artifactInfo.escapedName}") {
                    destinationDirectory.set(versionDir)
                    archiveBaseName.set(artifactInfo.name)
                    this.archiveVersion.set(version)

                    onlyIf {
                        val paths = extractedPaths.paths[artifactInfo] ?: return@onlyIf false
                        paths.forEach { path ->
                            val file = File(path)
                            from(if (file.isFile) project.zipTree(file) else file) {
                                exclude("META-INF/*.MF")
                                exclude("META-INF/*.SF")
                                exclude("META-INF/*.DSA")
                                exclude("META-INF/*.RSA")
                            }
                        }
                        return@onlyIf true
                    }
                })

                artifact(sourceJar) {
                    classifier = "sources"
                }
            }

            if (BINTRAY_ENABLED) {
                val isPublishedToBintray = isPublishedToBintray(
                    this@publication.groupId,
                    this@publication.artifactId,
                    this@publication.version
                )
                if (isPublishedToBintray) {
                    logger.warn("Gradle $version: ${artifactInfo.name} has already been published to Bintray")
                    tasks.named("publish${escapedVersion.capitalize()}-${artifactInfo.escapedName}PublicationToBintrayRepository") {
                        if (!REBUILD
                            && name !in gradle.startParameter.taskNames
                            && "publish${escapedVersion.capitalize()}ToBintrayRepository" !in gradle.startParameter.taskNames
                            && "publish-${artifactInfo.escapedName}ToBintrayRepository" !in gradle.startParameter.taskNames
                        ) {
                            disableTask()
                        }
                    }
                }
            }

            if (OSSRH_ENABLED) {
                val isPublishedToMavenCentral = isPublishedToMavenCentral(
                    this@publication.groupId,
                    this@publication.artifactId,
                    this@publication.version
                )
                if (isPublishedToMavenCentral) {
                    logger.warn("Gradle $version: ${artifactInfo.name} has already been published to Maven Central")
                    tasks.named("publish${escapedVersion.capitalize()}-${artifactInfo.escapedName}PublicationToOssrhRepository") {
                        if (!REBUILD
                            && name !in gradle.startParameter.taskNames
                            && "publish${escapedVersion.capitalize()}ToOssrhRepository" !in gradle.startParameter.taskNames
                            && "publish-${artifactInfo.escapedName}ToOssrhRepository" !in gradle.startParameter.taskNames
                        ) {
                            disableTask()
                        }
                    }
                }
            }

            Unit
        }
    }

    arrayOf(
        if (BINTRAY_ENABLED) "BintrayRepository" else null,
        if (OSSRH_ENABLED) "OssrhRepository" else null,
        "MavenLocal"
    ).filterNotNull().forEach { taskSuffix ->
        tasks.create("publish${escapedVersion.capitalize()}To$taskSuffix") {
            group = PUBLISH_TASK_GROUP
            artifactInfos.forEach { artifactInfo ->
                dependsOn("publish${escapedVersion.capitalize()}-${artifactInfo.escapedName}PublicationTo$taskSuffix")
            }
        }
    }
}

arrayOf(
    if (BINTRAY_ENABLED) "BintrayRepository" else null,
    if (OSSRH_ENABLED) "OssrhRepository" else null,
    "MavenLocal"
).filterNotNull().forEach { taskSuffix ->
    allArtifactInfos.forEach forEachArtifactInfo@{ artifactInfo ->
        tasks.create("publish-${artifactInfo.escapedName}To$taskSuffix") {
            group = PUBLISH_TASK_GROUP
            gradleVersions.forEach forEachVersion@{ gradleVersion ->
                val escapedVersion = escapeVersion(gradleVersion.version)
                val dependentTaskName = "publish${escapedVersion.capitalize()}-${artifactInfo.escapedName}PublicationTo$taskSuffix"
                if (dependentTaskName in tasks) {
                    dependsOn(dependentTaskName)
                }
            }
        }
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

data class ExtractedPaths(
    val distributionPath: File,
    val paths: Map<ArtifactInfo, List<String>>
)

fun extractPaths(artifactInfos: List<ArtifactInfo>, gradleVersion: GradleVersion): ExtractedPaths {
    val version = gradleVersion.version
    val pathProjectDir = buildDir.resolve("tmp/" + version).forceDeleteRecursively()
    projectDir.resolve("gradle").copyRecursively(pathProjectDir.resolve("gradle").createParentDirectories())
    Properties().apply {
        load(projectDir.resolve("gradle/wrapper/gradle-wrapper.properties"))
        val baseDistributionUrl = (getenv("GRADLE_DISTRIBUTIONS_MIRROR").nullIfEmpty()
            ?: "https://services.gradle.org/distributions/").trimEnd('/')
        put("distributionUrl", "$baseDistributionUrl/gradle-$version-all.zip")
        store(pathProjectDir.resolve("gradle/wrapper/gradle-wrapper.properties").createParentDirectories())
    }
    pathProjectDir.resolve("build.gradle").createParentDirectories().writer().use { writer ->
        writer.writeln("println 'distribution: ' + gradle.gradleHomeDir.absolutePath")
        writer.writeln("")
        writer.writeln("def conf")
        artifactInfos.filter { it.dependenciesMethodName != null }.forEach {
            writer.writeln("")
            writer.writeln("println 'name: ${it.name}'")
            writer.writeln("conf = configurations.create('${it.name}')")
            writer.writeln("conf.dependencies.add(dependencies.${it.dependenciesMethodName}())")
            writer.writeln("conf.files.forEach { println it.absolutePath }")
            writer.writeln("println ''")
        }
        writer.writeln("")
        writer.writeln("tasks.create('emptyTask')")
        Unit
    }

    val os = OperatingSystem.current()
    val scriptFile = os.getScriptName("gradlew").let { name ->
        val target = pathProjectDir.resolve(name).absoluteFile.createParentDirectories()
        projectDir.resolve(name).copyRecursively(target)
        target.setExecutable(true)
        return@let target
    }
    val outputFile = pathProjectDir.resolve("paths.out")
    val errorOutputFile = pathProjectDir.resolve("paths.err")
    ProcessBuilder(scriptFile.path, "--quiet", "emptyTask")
        .directory(pathProjectDir)
        .apply { environment()["GRADLE_USER_HOME"] = DEFAULT_GRADLE_USER_HOME }
        .redirectOutput(outputFile.createParentDirectories())
        .redirectError(errorOutputFile.createParentDirectories())
        .start()
        .apply {
            if (!waitFor(5, MINUTES) || exitValue() != 0) {
                val delim = "-".repeat(20)
                println("$delim\nExtracting paths failed:\nStandart output:")
                if (outputFile.isFile) println(outputFile.readText())
                println("$delim\nError output:")
                if (errorOutputFile.isFile) println(errorOutputFile.readText())
                println(delim)
                outputFile.deleteRecursively()
                throw GradleException("Path extraction failed")
            }
        }
    if (!gradleVersion.equals(GradleVersion.current())) {
        ProcessBuilder(scriptFile.path, "--quiet", "--stop")
            .directory(pathProjectDir)
            .apply { environment()["GRADLE_USER_HOME"] = DEFAULT_GRADLE_USER_HOME }
            .redirectOutput(PIPE)
            .redirectError(PIPE)
            .start()
            .waitFor(5, MINUTES)
    }

    val paths = mutableMapOf<ArtifactInfo, MutableList<String>>()
    lateinit var distributionPath: File
    outputFile.bufferedReader().use { reader ->
        while (true) {
            val line = reader.readLine()?.trim() ?: return@use
            if (line.startsWith("distribution: ")) {
                distributionPath = File(line.substring("distribution: ".length))
                logger.lifecycle("Distribution path: {}", distributionPath)
                continue
            }

            if (!line.startsWith("name: ")) continue
            val name = line.substring("name: ".length)
            val currentPaths = mutableListOf<String>()
            while (true) {
                val path = reader.readLine()?.trim() ?: return@use
                if (path.isEmpty()) break
                currentPaths.add(path)
            }
            val artifactInfo = artifactInfos.first { it.name == name }
            paths[artifactInfo] = currentPaths
        }
    }

    val libsPath = distributionPath.resolve("lib").toPath().toAbsolutePath().normalize()
    artifactInfos.forEach { artifactInfo ->
        val includeLibFilesPattern = artifactInfo.includeLibFilesPattern ?: return@forEach
        val excludeLibFilesPattern = artifactInfo.excludeLibFilesPattern
        libsPath.walk()
            .filter(Path::isRegularFile)
            .map(Path::toAbsolutePath)
            .map(Path::normalize)
            .filter { absPath ->
                val relativePath = libsPath.relativize(absPath).toString()
                includeLibFilesPattern.matches(relativePath)
                    && (excludeLibFilesPattern == null || !excludeLibFilesPattern.matches(relativePath))
            }
            .forEach { absPath ->
                paths.getOrPut(artifactInfo, ::mutableListOf).add(absPath.toFile().path)
            }
    }

    paths.forEach { artifactInfo, currentPaths ->
        artifactInfo.allDeps.forEach { paths[it]?.let(currentPaths::removeAll) }
        currentPaths.forEach { logger.lifecycle("{}: {}", artifactInfo.name, it) }
    }

    paths.values.removeIf(Collection<*>::isEmpty)

    return ExtractedPaths(distributionPath, paths)
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

fun Task.disableTask() {
    enabled = false
    onlyIf { false }
    setDependsOn(emptyList<Any>())

    val inputs = this.inputs
    inputs.javaClass.unwrapGradleGenerated()
        .getDeclaredField("registeredFileProperties")
        .apply { isAccessible = true }
        .get(inputs)
        .uncheckedCast<MutableIterable<Any>>()
        .iterator()
        .run {
            while (hasNext()) {
                next()
                remove()
            }
        }
}

fun escapeVersion(version: String) = version.splitToSequence('.')
    .map { if (it.length == 1) "-$it" else it }
    .joinToString("-")
